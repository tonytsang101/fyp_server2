<?php

namespace App\Admin\Controllers;

use App\User_photos;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\User;
use Intervention\Image\Facades\Image;

class UserPhotosController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\User_photos';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User_photos);
        $grid->model()->orderBy('user_id', 'asc');
        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('photo_path', __('Photo path'));
        $grid->column('small_photo_path', __('Small Photo path'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User_photos::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('photo_path', __('Photo path'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User_photos);
        $form->hidden('id');
        $form->select('user_id', __('Username'))->options(User::all()->pluck('username','id'))->rules('required');
        $form->image('photo_path', __('Photo'))->name(function (){return Str::random(10).'.png';})->rules('required');
        $form->saved(function (Form $form){
            $photo = User_photos::find($form->model()->id);
            $photo_name = substr($photo->photo_path,0,10);
            if (!Str::contains($photo->photo_path,'storage/')){
                $photo->photo_path = 'storage/'.$photo->photo_path;
                $photo->save();
            }
            $photo = User_photos::find($form->model()->id);
            $img = Image::make($photo->photo_path)->resize(240, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/'.$photo_name.'_small.png');
            $photo->small_photo_path = 'storage/'.$photo_name.'_small.png';
            $photo->save();
        });
        return $form;
    }
}
