<?php

namespace App\Http\Controllers;

use App\User;
use App\User_photos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;


class ApiController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'username' => 'required|string|unique:users',
            'password' => 'required|string',
            'name' => 'string',
            'phone' => 'required|string',
        ]);
        $user = new User();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->save();
        return response()->json([
            'message' => 'Successfully created user! Please Log in!'
        ]);
    }
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            //            'remember_me' => 'boolean'
        ]);
        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 200);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function upimg(Request $request)
    {
        $user = User::where('id', $request->user()->id)->first();
        $img_name = $user->id . '_' . $user->name . '_' . Str::random(10) . '.png';
        Storage::disk('public')->put($img_name,  base64_decode($request->image));

        $user_photo = new User_photos();
        $user_photo->user_id = $user->id;
        $user_photo->photo_path = 'storage/' . $img_name;
        $user_photo->save();
        return response()->json([
            'message' => 'Success!'
        ]);
    }

    public function reviewimg(Request $request)
    {
        $user_photos = User_photos::where('user_id', $request->user()->id)->get()->toArray();
        $photo_list = [];
        foreach ($user_photos as $data) {
            $photo_list[] = $data['photo_path'];
        }
        return response()->json(['photo_list' => $photo_list]);
    }
}
