<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'ApiController@login');
    Route::post('signup', 'ApiController@signup');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('upimg', 'ApiController@upimg');
        Route::get('reviewimg', 'ApiController@reviewimg');
    });
});

